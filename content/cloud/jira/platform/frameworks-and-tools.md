---
title: Frameworks and tools
platform: cloud
product: jiracloud
category: devguide
subcategory: intro
aliases:
- /jiracloud/frameworks-and-tools.html
- /jiracloud/frameworks-and-tools.md
date: "2016-10-09"
---
{{< include path="docs/content/cloud/connect/concepts/jira-frameworks-and-tools.snippet.md">}}