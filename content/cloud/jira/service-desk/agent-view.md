---
title: Agent view 
platform: cloud
product: jsdcloud
category: reference
subcategory: modules
aliases:
    - /jiracloud/jira-service-desk-modules-agent-view-39988009.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39988009
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39988009
confluence_id: 39988009
date: "2016-07-01"
---
# Agent view

This pages lists the JIRA Service Desk modules for the agent view. These can be used to inject new groups (tabs) in the JIRA Service Desk agent view.

## Queue group

A group of [serviceDeskQueues](#queue).

#### Module type
`serviceDeskQueueGroups`

#### Screenshot
<img src="../images/sd-queue-content.png"/> 

#### Sample JSON
``` json
"modules": {
    "serviceDeskQueueGroups": [ 
        { 
            "key": "my-custom-queues-section", 
            "name": { 
                "value": "My custom queues section" 
            }
        }
    ]
}
```
{{% note %}}The group will not be displayed if there are no [serviceDeskQueues](#queue) that reference it in the "group" property or if all the queues that reference it are not displayed because of conditions.{{% /note %}}

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`name`

-   **Type**: [i18n Property]
-   **Required**: yes
-   **Description**: A human readable name.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

----

## Queue

A queue in the queues sidebar.

#### Module type
`serviceDeskQueues`

#### Screenshot
<img src="../images/sd-queue-content.png"/>

#### Sample JSON
``` json
"modules": {
    "serviceDeskQueues": [ 
        { 
            "key": "my-custom-queue", 
            "name": { 
                "value": "My custom queue" 
            }, 
            "group": "my-custom-queues-section", 
            "url": "/sd-queue-content" 
        }
    ]
}
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`name`

-   **Type**: [i18n Property]
-   **Required**: yes
-   **Description**: A human readable name.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`group`

-   **Type**: `string`
-   **Required**: no
-   **Description**: References the key of a [serviceDeskQueueGroup](#queue-group). If this property is not provided, the queue will appear in a generic "Add-ons" group.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

----

## Report group

A group of [serviceDeskReports](#report).

#### Module type
`serviceDeskReportGroups`

#### Screenshot
<img src="../images/sd-report-content.png"/> 

#### Sample JSON
``` json
"modules": {
    "serviceDeskReportGroups": [ 
        { 
            "key": "my-custom-reports-section", 
            "name": { 
                "value": "My custom reports section" 
            }
        }
    ]
}
```
{{% note %}}The group will not be displayed if there are no [serviceDeskReports](#report) that reference it in the "group" property or if all the reports that reference it are not displayed because of conditions.{{% /note %}}

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`name`

-   **Type**: [i18n Property]
-   **Required**: yes
-   **Description**: A human readable name.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

----

## Report

A report in the reports sidebar.

#### Module type
`serviceDeskReports`

#### Screenshot
<img src="../images/sd-report-content.png"/>

#### Sample JSON
``` json
"modules": {
 "serviceDeskReports": [ 
     { 
         "key": "my-custom-report", 
         "name": { 
             "value": "My custom report" 
         }, 
         "group": "my-custom-reports-section", 
         "url": "/sd-report-content" 
        }
    ]
}
```

#### Properties

`key`

-   **Type**: `string (^[a-zA-Z0-9-]+$)`
-   **Required**: yes
-   **Description**: A key to identify this module. This key must be unique relative to the add on, with the exception of Confluence macros: their keys need to be globally unique. Keys must only contain alphanumeric characters and dashes.

`name`

-   **Type**: [i18n Property]
-   **Required**: yes
-   **Description**: A human readable name.

`weight`

-   **Type**: `integer`
-   **Default**: 100
-   **Description**: Determines the order in which the web item appears in the menu or list. The "lightest" weight (i.e., lowest number) appears first, rising relative to other items, while the "heaviest" weights sink to the bottom of the menu or list.<br>Built-in web items have weights that are incremented by numbers that leave room for additional items, such as by 10 or 100. Be mindful of the weight you choose for your item, so that it appears in a sensible order given existing items.

`group`

-   **Type**: `string`
-   **Required**: no
-   **Description**: References the key of a [serviceDeskReportGroup](#report-group). If this property is not provided, the report will appear in a generic "Add-ons" group.

`url`

-   **Type**: `string`, `uri-template`
-   **Required**: yes
-   **Description**:  The URL of the add-on resource that provides the content. This URL must be relative to the add-on's baseUrl. Your add-on can receive [additional context] from the application by using variable tokens in the URL attribute.

`conditions`

-   **Type**: [ [single condition], [composite condition], ... ]
-   **Description**: [Conditions] can be added to display only when all the given conditions are true.

  [i18n Property]: /cloud/jira/platform/connect/modules/i18n-property
  [single condition]: /cloud/jira/platform/connect/modules/single-condition
  [composite condition]: /cloud/jira/platform/connect/modules/composite-condition
  [Conditions]: /cloud/jira/service-desk/conditions
  [additional context]: /cloud/jira/service-desk/context-parameters
